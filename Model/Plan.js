const {Sequelize, sequelize} = require('./sequelize');

module.exports = sequelize.define('Plan', {
    // attributes
    id: {
      type: Sequelize.STRING,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    ammount: {
      type: Sequelize.FLOAT,
      allowNull: false
    },
    frequency: {
      type: Sequelize.STRING,
      allowNull: false
    },
  });
