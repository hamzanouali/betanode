const {Sequelize, sequelize} = require('./sequelize');

module.exports = sequelize.define('Payment', {
    // attributes
    user_id: {
      type: Sequelize.STRING,
      allowNull: false,
      primaryKey: true
    },
    create_time: {
      type: Sequelize.STRING,
      allowNull: false
    },
    payer: {
      type: Sequelize.TEXT,
      get: function () {
          return JSON.parse(this.getDataValue('payer'));
      },
      set: function (value) {
          this.setDataValue('payer', JSON.stringify(value));
      },
      allowNull: true
    }, 
    amount: {
      type: Sequelize.STRING,
      allowNull: false
    },
    currency_code: {
      type: Sequelize.STRING,
      allowNull: false
    },
    payee: {
      type: Sequelize.TEXT,
      get: function () {
          return JSON.parse(this.getDataValue('payee'));
      },
      set: function (value) {
          this.setDataValue('payee', JSON.stringify(value));
      },
      allowNull: true
    }
  });
