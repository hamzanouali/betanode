const {Sequelize, sequelize} = require('./sequelize');

module.exports = sequelize.define('BillingAgreement', {
    // attributes
    id: {
      type: Sequelize.STRING,
      allowNull: false,
      primaryKey: true
    },
    user_id: {
      type: Sequelize.STRING,
      allowNull: false
    },
    payer_email: {
      type: Sequelize.STRING,
      allowNull: false
    },
    
  });
