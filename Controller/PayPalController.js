const debug = require('debug')('app:PayPalController');
const { paypal, myPaypalHelper } = require('../PayPal_modules');
const axios = require('axios');
const qs = require('qs');
const Plan = require('../Model/Plan'); 
const BillingAgreement = require('../Model/BillingAgreement'); 


module.exports = {

  async createPlan(req, res) {
    res.render('PayPal/CreatePlan');
  },

  async createBillingAgreement(req, res) {
    debug('I am in createBillingAgreement method.');

    Plan.findAll()
    .then(plans => {
      res.render('PayPal/CreateBillingAgreement', { plans });    
    })
    .catch(err => {
      res.status(500).end('500 error');
    })
  },

  async createBilling(req, res) {
    var billingPlan = req.body.id;
    var billingAgreementAttributes;
    const startDate = new Date((new Date().getTime() + 60 * 60 * 24 * 1000)).toISOString();

    billingAgreementAttributes = {
      name: 'Standard Membership',
      description: 'Food of the World Club Standard Membership',
      start_date: startDate,
      plan: {
        id: billingPlan
      },
      payer: {
        payment_method: 'paypal'
      },
      "override_merchant_preferences": {
        "return_url": "http://localhost:7000/user/paypal/billing/success",
        "cancel_url": "http://localhost:7000/cancel",
        "auto_bill_amount": "YES",
        "initial_fail_amount_action": "CONTINUE",
        "max_fail_attempts": "5"
      }
    };

    var links = {};

    // Use activated billing plan to create agreement
    paypal.billingAgreement.create(billingAgreementAttributes, function (error, billingAgreement){
      if (error){
        console.error(JSON.stringify(error));
        throw error;
      } else {
        // Capture HATEOAS links
        billingAgreement.links.forEach(function(linkObj){
          links[linkObj.rel] = {
            href: linkObj.href,
            method: linkObj.method
          };
        })

        console.log(billingAgreement.links);
        // If redirect url present, redirect user
        if (links.hasOwnProperty('approval_url')){
          //REDIRECT USER TO links['approval_url'].href
          res.redirect(links['approval_url'].href);
        } else {
          console.error('no redirect URI present');
        }
      }
    });
  },

  async executeBilling(req, res) {
    var token = req.query.token;

    paypal.billingAgreement.execute(token, {}, async (error, billingAgreement) => {
      if (error){
        console.error(JSON.stringify(error));
        throw error;
      } else {

        let email = 'inherit';

        if(billingAgreement.payer && 
        billingAgreement.payer.payer_info && 
        billingAgreement.payer.payer_info.email) {
          email = billingAgreement.payer.payer_info.email;
        }

        try {
          req.cookies.auth = JSON.parse(req.cookies.auth);
        } catch (error) {
        }
        
        try {
          BillingAgreement.create({
            id: billingAgreement.id,
            user_id: req.cookies.auth.id,
            email: email,
          });  
        } catch (error) {
          throw error;
        }

        try {
          await User.update({
            where: { id: req.cookies.auth.id },
            defaults: {
              subscribtion: 'paid'
            }
          });
        } catch (error) {
          throw error;
        }

        res.json(billingAgreement);
      }
    });
  },

  /**
   * creates a new plan
   * @param {express request} req 
   * @param {express response} res 
   */
  async create(req, res) {
    req.body.billingPlanAttribs = {
      "name": "Plan with Regular Payment Definitions",
      "description": "Plan with regular definitions.",
      "type": "FIXED",
      "payment_definitions": [
        {
          "name": "Regular payement",
          "type": "REGULAR",
          "frequency": "MONTH",
          "frequency_interval": "1",
          "amount": {
            "value": "19.99",
            "currency": "USD"
          },
          "cycles": "12",
        }
      ],
      "merchant_preferences": {
        "return_url": "http://localhost:7000/user/paypal/billing/success",
        "cancel_url": "http://localhost:7000/cancel",
        "auto_bill_amount": "YES",
        "initial_fail_amount_action": "CONTINUE",
        "max_fail_attempts": "5"
      }
    };

    paypal.billingPlan.create(req.body.billingPlanAttribs, function (error, billingPlan){
      var billingPlanUpdateAttributes;

      if (error){
        console.error(JSON.stringify(error));
        throw error;
      } else {
        // Create billing plan patch object
        billingPlanUpdateAttributes = [{
          op: 'replace',
          path: '/',
          value: {
            state: 'ACTIVE'
          }
        }];

        // Activate the plan by changing status to active
        paypal.billingPlan.update(billingPlan.id, billingPlanUpdateAttributes, function(error, response){
          if (error){
            console.error(JSON.stringify(error));
            throw error;
          } else {
            
            Plan.create({
              id        : billingPlan.id,
              name      : billingPlan.name,
              ammount   : billingPlan.payment_definitions[0].amount.value,
              frequency : billingPlan.payment_definitions[0].frequency
            })
            .then(plan => {
              res.end('Your plan has been created successfully!');
            })
            .catch(err => {
              throw err;
            });
            //createBillingAgreement(billingPlan.id);
          }
        });
      }
    });

  },

  /**
   * list all the created plans
   */
  async list(req, res) {
    
    const accessToken = await this.getAccessToken();
    debug(accessToken);

    const { plans, error } = await myPaypalHelper.listPlans(accessToken);

    if(error) {
      throw error;
    }

    res.json(plans);
  },

  /**
   * gets paypal api access token
   * @todo: make it more safe
   */
  async getAccessToken() {
    const url = 'https://api.sandbox.paypal.com/v1/oauth2/token';
    const data = {
      grant_type: 'client_credentials'
    };
    const auth = {
      username: 'AQ4-TbrLQrVqdAIANNt_FKuTxbq81Kpybsn9Qk6ZHRKJsHf_kLvaYb2DhrO9rJsf5Af8UfK4hTOCFgrD',
      password: 'ENmEHGLKjbCB8FwyzTSGnkfvfAyKxLx_ZP6RXl08nc91LbHffy7hbhz-HDZW7vkZLHXnzpqtjq69lRqF'
    };
    const options = {
      method: 'post',
      headers: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      withCredentials: true,
      data: qs.stringify(data),
      auth: auth,
      url,
    };

    return new Promise((resolve, reject) => {
      axios(options)
      .then(response => {

        resolve({
          'access-token': response.data.access_token,
          'expired': (response.data.expires_in > Date.now() / 1000) ? true : false
        });

      })
      .catch(err => {
        reject(err);
      });
    });
  }

}