const debug = require('debug')('app:Controller');
const zipafolder = require('zip-a-folder');
const zip = new require('node-zip')();
const scope = require('scope-css');
const sass = require('node-sass');
const Cryptr = require('cryptr');
const pretty = require('pretty');
const Joi = require('@hapi/joi');
const ncp = require('ncp').ncp;
const path = require('path');
const fs = require('fs') ;

ncp.limit = 16;


const cryptr = new Cryptr('secret_myTotalySecretKey');

module.exports = {

  async form(req, res) {
    res.send('okk');
  },

  async renderform(req, res) {
    res.render('form');
  },

  /**
   * uploads and return backup as json
   * @param {*} req 
   * @param {*} res 
   */
  uploadBackup(req, res) {
    const file = req.file

    if (!file) {
      return res.status(400).end('Please upload file!');     
    }

    directory = `./static/${req.cookies.auth.id}`;

    fs.mkdir(directory, { recursive: true }, err => {

      if(err) throw err;

      // store encrypted file's content
      fs.writeFile(`${directory}/backup.json`,file.buffer.toString() ,(err) => {
        if(err) throw err;
  
        // decrypt file content
        const decryptedContent = cryptr.decrypt(file.buffer.toString());
        // return backup as json response 
        res.json(decryptedContent);
      });
    });
  },
  
  async userIP(req, res) {
    const userIP = req.cookies.auth.id;
    res.end(userIP);
  },

  async create(req, res) {
    
    /**
     * Create for each workspace:
     * --- [SCSS] ---
     * Project/assets/scss/style.scss 
     * @todo Project/assets/scss/configuration.scss 
     * --- [CSS] ---
     * Project/assets/css/style.css
     * --- [HTML] ---
     * Project/workspace_name.html 
     */

    const userIP = req.cookies.auth.id;
    let localStorage = JSON.parse(req.body.localStorage)
    /* localStorage.workspaces = JSON.parse(localStorage).workspaces
    localStorage.SCSSCode = JSON.parse(localStorage).SCSSCode
    localStorage.assets = JSON.parse(localStorage).assets */
    let finished = []

    /**
     * get and prepare SCSS
     */
    // concatenate SCSS Code
    let SCSSCode = `
    html {
      padding: 0px;
      margin: 0px;
      height: 100%;
    }
    body {
      padding: 0px;
      margin: 0px;
      height: 100%;
    }
    `;
    SCSSCode += localStorage.SCSSConfiguration + '\n'

    for (const selector in localStorage.SCSSCode) {
      const content = localStorage.SCSSCode[selector];
      SCSSCode += `${selector}{\n${content}\n}\n`
    } 

    /**
     * Save SCSS
     */
    const directory = path.join(__dirname, '../../static/'+ userIP +'/assets');
    fs.mkdir(directory, { recursive: true }, err => {
      if(err) throw err;

      fs.writeFile(directory + '/style.scss', SCSSCode, err => {
        if(err) throw err;
        finished.push('SCSS')
        debug('downloading project - SCSS finished');
      })
    });
    /**
     * Compile SCSS, then save CSS
     */
    sass.render({ data: SCSSCode }, function(err, result) { 
      const directory = path.join(__dirname, '../../static/'+ userIP +'/assets');
      fs.mkdir(directory, { recursive: true }, err => {
        if(err) throw err;
        fs.writeFile(directory + '/style.css', result.css , err => {
          if(err) throw err;
          finished.push('compiledSCSS')
          debug('downloading project - SCSS compilation finished');
        })
      })
    }); 
  
    let counter = 0;
    
    for (const workspace_name in localStorage.workspaces) {
      // store workspace and its name  
      const workspace = localStorage.workspaces[workspace_name];

      /**
       * get and prepare html
       */
      let htmlAssetsLinks = ''
      for (const asset_name in localStorage.assets) {
        /**
         * copy default assets and past it inside user's folder
         */
        if(localStorage.assets[asset_name].to.includes('default_assets')) {
          /**
           * from default_assets
           */
          let directory = localStorage.assets[asset_name].to.split('/')       

          /**
           * extract directory without file name and without default_asset
           */
          directory.splice(directory.length-1, 1)
          directory.splice(0, 1)
          directory = directory.join('/')

          directory = path.join(__dirname, '../../static/'+ userIP + '/assets/' + directory)
          debug('directory: '+directory);
          fs.mkdir(directory, { recursive: true }, err => {
            if(err) throw err;

            let source = localStorage.assets[asset_name].to.split('/');
            // delete file name from string
            source.splice(source.length-1, 1);
            source = source.join('/');
            source = path.join(__dirname, `../../static/${source}`);
            debug('source: '+source);

            ncp(source, directory, function (err) {
              if (err) throw err;

            });
          });
          
        } 
        htmlAssetsLinks += `<link href="${localStorage.assets[asset_name].to}" rel="stylesheet">`
      } 

      let className
      let html
      try {
        className = workspace.htmlCode.className
      } catch (error) {
        throw error
      }
      try {
        html = workspace.htmlCode.html
      } catch (error) {
        throw error
      }
      let basicHTMLTemplate = `
      <!DOCTYPE html>
      <html lang="en">
      <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link href="./default_assets/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
        ${htmlAssetsLinks}
        <link href="./assets/style.css" rel="stylesheet">
        <title>${workspace_name}</title>
      </head> 
      <body class="${className}">
        ${html}
      </body>
      </html>`

      /**
       * makes html looks pretty
       */
      basicHTMLTemplate = pretty(basicHTMLTemplate);

      /**
       * Save HTML
       */
      fs.writeFile(path.join(__dirname, '../../static/'+ userIP +'/'+ workspace_name +'.html'), basicHTMLTemplate, err => {
        if(err) throw err;

        counter++;
      })

    }
    let interval_for_html = setInterval(() => {
      if(counter === Object.keys(localStorage.workspaces).length) {
        finished.push('html')
        debug('downloading project - html finished');
        clearInterval(interval_for_html)
      }
    },10) 

     /**
      * Save Backup json file
      */
    const encryptedContent = cryptr.encrypt(JSON.stringify(localStorage));
    fs.writeFile(path.join(__dirname, '../../static/'+ userIP +'/Backup.json'), encryptedContent, err => {
      if(err) throw err;
      finished.push('backup')
      debug('downloading project - backup finished');
    })

    /**
     * zip project folder
     */
    try {
      await zipafolder.zip(
        path.join(__dirname, '../../static/' + userIP), 
        path.join(__dirname, '../../static/'+ userIP +'.zip')
      );
    } catch (error) {
      throw error;
    }

    let interval_for_all = setInterval(() => {
      if(finished.includes('html') && finished.includes('SCSS') && finished.includes('backup') && finished.includes('compiledSCSS')) {
        clearInterval(interval_for_all)
        
        debug('downloading project - *** all finished ***');
        /**
         * download the project
         */
        res.end('download');
      }
    },10) 
  },

  /**
   * downloads the project
   * @param {*} req 
   * @param {*} res 
   */
  downloadProject(req, res) {
    const userIP = req.cookies.auth.id;
    res.download(path.join(__dirname, '../../static/'+ userIP +'.zip'));
  },

  /**
   * this should compile and save user scss customizations for a certain css 
   * framework or library
   */
  async customizeSCSS(req, res) {
    const userIP = req.cookies.auth.id;
    const framework = req.body.framework
    let scss = req.body.scss

    /**
     * fix directories
     */
    const regex = new RegExp("@import '", 'gms');
    let replacement = "\n@import '"+ path.join(__dirname, '../../node_modules/')
    replacement = replacement.replace(/(\\)/gms,'/')
    scss = scss.replace(regex, replacement)

    sass.render({ data: scss }, function(err, result) { 
      if(err) throw err

      const directory = path.join(__dirname, '../../static/'+ userIP +'/assets/bootstrap/css');
      fs.mkdir(directory, { recursive: true }, err => {
          if(err) throw err;
          
          fs.writeFile(directory + '/' + framework +'.min.css', result.css, (err) => {
            if(err) throw err
            
            res.end('true')
          });
      });

    });    

  },

  /**
   * copy selected assets from node_modules to user's project folder
   * @param {*} req 
   * @param {*} res 
   */
  async prepareAssets(req, res) {
    //console.log(Object.keys(req.body.assets) );
    const schema = Joi.object().keys({
        assets: Joi.required()
    });

    // Return result.
    const result = Joi.validate(req.body, schema);
    if(result.errors) { return res.status(402).json(result.errors); }

    const userIP = req.cookies.auth.id;
    let counter = 0
    for (const asset_name in req.body.assets) {
      const assetDirectories = req.body.assets[asset_name];

      /**
       * condition: do nothing to default assets
       * default assets must not be moved
       */
      if(assetDirectories.from.length && !assetDirectories.to.includes('default_assets')) {
        const copyFrom = path.join(__dirname, '../../node_modules/' + assetDirectories.from)
        const pasteTo = path.join(__dirname, '../../static/' + userIP + '/' + assetDirectories.to)
        const directory = pasteTo.slice(0, pasteTo.lastIndexOf('\\'))
      
        fs.mkdir(directory, { recursive: true }, err => {
          if(err) throw err;

            fs.copyFile(copyFrom, pasteTo, err => {
              if(err) throw err;
              
              counter++
            })
        });

      } else { 
        counter++ 
      }
    }

    let interval = setInterval(() => {
      if(counter === Object.keys(req.body.assets).length) {
        clearInterval(interval)
        res.end(userIP)
      }
    },10)
  },

  /**
   * deletes html file from user project folder
   * @param {*} req 
   * @param {*} res 
   */
  async deleteWorkspace(req, res) {
    const userIP = req.cookies.auth.id;
    fs.unlink(path.join(__dirname, '../../static/'+ userIP +'/'+ req.body.name +'.html'), err => {
      if(err) throw err;
      res.status(200).end('okk');
    });
  },

  /**
   * deletes html file from user project folder
   * @param {*} req 
   * @param {*} res 
   */
  async deleteAsset(req, res) {
    const userIP = req.cookies.auth.id;
    const rimraf = require("rimraf");
    
    if(!req.body.asset.to.includes('default_assets')) {
      const directory = path.join(__dirname, '../../static/'+ userIP + '/assets/' + req.body.asset.name);
      debug('delete directory: '+ directory);
      rimraf(directory,{}, function (err) { 
        if(err) throw err;
        res.status(200).end('okk');
      });
    } else {
      res.status(200).end('okk');
    }
  },

}