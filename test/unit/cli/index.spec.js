const { exec } = require('child_process');
const { execSync } = require('child_process');
const fs = require('fs')
const log = require('../../../Core/CLI/log')
const rimraf = require('rimraf')

// calling and runing test
require('./controllerCommands.spec')()
require('./modelCommands.spec')()
require('./middlewareCommands.spec')()
require('./routeCommands.spec')()

describe ('cli helpers: ./Core/CLI/commands/helpers/', () => {

    describe ('handleDirectory methode', () => {

      let file_name = 'file.txt'
      let file_name_and_new_folder = 'Folder/file.txt'
      let file_name_and_new_folder_with_slash = '/Folder/file.txt'
      let directory = './Controller/'
      let result_directory = './Controller/Folder/'

      afterEach (() => {

        // delete the created direcotories
        rimraf(result_directory, err => {
          if(err) {
            throw err
          }
        })

      })

      it (`handleDirectory('${file_name_and_new_folder}', '${directory}') should create the directory if not exist.`, (done) => {

        const handleDirectory = require('../../../Core/CLI/commands/helpers/handleDirectory')

        // this method won't create the file
        // handled object should contain the full directory and file name
        const handledObject = handleDirectory(file_name_and_new_folder, directory)

        expect(handledObject.file_name).toBe(file_name)
        expect(handledObject.directory).toBe(result_directory)

        // check whether the directory exists
        fs.exists(handledObject.directory, result => {

          expect(result).toBe(true)

          done()

        })

      })

      it (`handleDirectory('/${file_name_and_new_folder}', '${directory}') should create the directory if not exist.`, (done) => {
        
        // UPDATE: adding slash to file_name_and_new_folder
        file_name_and_new_folder = `/${file_name_and_new_folder}`

        const handleDirectory = require('../../../Core/CLI/commands/helpers/handleDirectory')

        // this method won't create the file
        // handled object should contain the full directory and file name
        const handledObject = handleDirectory(file_name_and_new_folder, directory)

        expect(handledObject.file_name).toBe(file_name)
        expect(handledObject.directory).toBe(result_directory)

        // check whether the directory exists
        fs.exists(handledObject.directory, result => {

          expect(result).toBe(true)

          done()

        })

      })


    })

  })