/**
 * Runs all the tests related to Middleware commands
 */
module.exports = () => {

const { exec } = require('child_process');
const { execSync } = require('child_process');
const fs = require('fs')
const log = require('../../../Core/CLI/log')

// the name of Middleware to be created for testing
let Middleware = 'TestingMiddleware'

let directory = './Middleware/'



describe('Middleware Commands', () => { 

  describe ('Create Commands: ', () => {

    afterEach((done) => {
      // delete the created file
      fs.unlink(`${directory}${Middleware}.js`, (err) => {
        // error case
        if(err) throw err

        done()

      })
    })

    it ('cli create-middleware TestingMiddleware', (done) => {

      // execute commande
      exec(`cli create-middleware ${Middleware}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${Middleware}.js`, (result) => {
          // assert result
          expect(result).toBe(true)
          done()
        })

      })

    })

    it ('cli cmi TestingMiddleware', (done) => {

      // execute commande
      exec(`cli cmi ${Middleware}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${Middleware}.js`, (result) => {
          // assert result
          expect(result).toBe(true)
          done()
        })

      })
    
    })

    it ('cli create-middleware Folder/TestingMiddleware', (done) => {

      // updating Middleware
      Middleware = 'Folder/TestingMiddleware'

      // execute commande
      exec(`cli create-middleware ${Middleware}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${Middleware}.js`, (result) => {
          // assert result
          expect(result).toBe(true)
          done()
        })

      })
    
    })

    it ('cli create-middleware /Folder/TestingMiddleware', (done) => {

      // updating Middleware
      Middleware = '/Folder/TestingMiddleware'

      // execute commande
      exec(`cli create-middleware ${Middleware}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${Middleware}.js`, (result) => {
          // assert result
          expect(result).toBe(true)
          done()
        })

      })
    
    })

    it ('cli cmi Folder/TestingMiddleware', (done) => {

      // updating Middleware
      Middleware = 'Folder/TestingMiddleware'

      // execute commande
      exec(`cli cmi ${Middleware}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${Middleware}.js`, (result) => {
          // assert result
          expect(result).toBe(true)
          done()
        })

      })
    
    })

  })


  describe ('Delete Commands: ', () => {

    beforeEach (() => {

      /* 
      before each test, create a new directory with a new file
      in order to test deleting these files 
      */

      execSync(`cli cmi ${Middleware}`)
      execSync(`cli cmi Folder/${Middleware}`)

    })

    it ('cli delete-middleware TestingMiddleware', (done) => {

      // execute commande
      exec(`cli delete-middleware ${Middleware}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${Middleware}.js`, (result) => {
          // assert result
          expect(result).toBe(false)
          done()
        })

      })
    
    })

    it ('cli delete-middleware Folder/TestingMiddleware', (done) => {

      // updating Middleware
      Middleware = 'Folder/TestingMiddleware'

      // execute commande
      exec(`cli delete-middleware ${Middleware}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${Middleware}.js`, (result) => {
          // assert result
          expect(result).toBe(false)
          done()
        })

      })
    
    })

    it ('cli dmi /Folder/TestingMiddleware', (done) => {

      // updating Middleware
      Middleware = '/Folder/TestingMiddleware'

      // execute commande
      exec(`cli dmi ${Middleware}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${Middleware}.js`, (result) => {
          // assert result
          expect(result).toBe(false)
          done()
        })

      })
    
    })

    it ('cli dmi TestingMiddleware', (done) => {

      // execute commande
      exec(`cli dmi ${Middleware}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${Middleware}.js`, (result) => {
          // assert result
          expect(result).toBe(false)
          done()
        })

      })
    
    })

  })

})

}