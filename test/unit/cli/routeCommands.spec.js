/**
 * Runs all the tests related to Route commands
 */
module.exports = () => {

const { exec } = require('child_process');
const { execSync } = require('child_process');
const fs = require('fs')
const log = require('../../../Core/CLI/log')

// the name of Route to be created for testing
let Route = 'TestingRoute'

let directory = './Route/'



describe('Route Commands', () => { 

  describe ('Create Commands: ', () => {

    afterEach((done) => {
      // delete the created file
      fs.unlink(`${directory}${Route}.js`, (err) => {
        // error case
        if(err) throw err

        done()

      })
    })

    it ('cli create-route TestingRoute', (done) => {

      // execute commande
      exec(`cli create-route ${Route}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${Route}.js`, (result) => {
          // assert result
          expect(result).toBe(true)
          done()
        })

      })

    })

    it ('cli cr TestingRoute', (done) => {

      // execute commande
      exec(`cli cr ${Route}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${Route}.js`, (result) => {
          // assert result
          expect(result).toBe(true)
          done()
        })

      })
    
    })

    it ('cli create-route Folder/TestingRoute', (done) => {

      // updating Route
      Route = 'Folder/TestingRoute'

      // execute commande
      exec(`cli create-route ${Route}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${Route}.js`, (result) => {
          // assert result
          expect(result).toBe(true)
          done()
        })

      })
    
    })

    it ('cli create-route /Folder/TestingRoute', (done) => {

      // updating Route
      Route = '/Folder/TestingRoute'

      // execute commande
      exec(`cli create-route ${Route}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${Route}.js`, (result) => {
          // assert result
          expect(result).toBe(true)
          done()
        })

      })
    
    })

    it ('cli cr Folder/TestingRoute', (done) => {

      // updating Route
      Route = 'Folder/TestingRoute'

      // execute commande
      exec(`cli cr ${Route}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${Route}.js`, (result) => {
          // assert result
          expect(result).toBe(true)
          done()
        })

      })
    
    })

  })


  describe ('Delete Commands: ', () => {

    beforeEach (() => {

      /* 
      before each test, create a new directory with a new file
      in order to test deleting these files 
      */

      execSync(`cli cr ${Route}`)
      execSync(`cli cr Folder/${Route}`)

    })

    it ('cli delete-route TestingRoute', (done) => {

      // execute commande
      exec(`cli delete-route ${Route}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${Route}.js`, (result) => {
          // assert result
          expect(result).toBe(false)
          done()
        })

      })
    
    })

    it ('cli delete-route Folder/TestingRoute', (done) => {

      // updating Route
      Route = 'Folder/TestingRoute'

      // execute commande
      exec(`cli delete-route ${Route}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${Route}.js`, (result) => {
          // assert result
          expect(result).toBe(false)
          done()
        })

      })
    
    })

    it ('cli dr /Folder/TestingRoute', (done) => {

      // updating Route
      Route = '/Folder/TestingRoute'

      // execute commande
      exec(`cli dr ${Route}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${Route}.js`, (result) => {
          // assert result
          expect(result).toBe(false)
          done()
        })

      })
    
    })

    it ('cli dr TestingRoute', (done) => {

      // execute commande
      exec(`cli dr ${Route}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${Route}.js`, (result) => {
          // assert result
          expect(result).toBe(false)
          done()
        })

      })
    
    })

  })

})

}