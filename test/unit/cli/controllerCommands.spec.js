/**
 * Runs all the tests related to controller commands
 */
module.exports = () => {

const { exec } = require('child_process');
const { execSync } = require('child_process');
const fs = require('fs')
const log = require('../../../Core/CLI/log')

// the name of controller to be created for testing
let controller = 'TestingController'

let directory = './Controller/'



describe('Controller Commands', () => { 

  describe ('Create Commands: ', () => {

    afterEach((done) => {
      // delete the created file
      fs.unlink(`${directory}${controller}.js`, (err) => {
        // error case
        if(err) throw err

        done()

      })
    })

    it ('cli create-controller TestingController', (done) => {

      // execute commande
      exec(`cli create-controller ${controller}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${controller}.js`, (result) => {
          // assert result
          expect(result).toBe(true)
          done()
        })

      })

    })

    it ('cli cc TestingController', (done) => {

      // execute commande
      exec(`cli cc ${controller}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${controller}.js`, (result) => {
          // assert result
          expect(result).toBe(true)
          done()
        })

      })
    
    })

    it ('cli create-controller Folder/TestingController', (done) => {

      // updating controller
      controller = 'Folder/TestingController'

      // execute commande
      exec(`cli create-controller ${controller}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${controller}.js`, (result) => {
          // assert result
          expect(result).toBe(true)
          done()
        })

      })
    
    })

    it ('cli create-controller /Folder/TestingController', (done) => {

      // updating controller
      controller = '/Folder/TestingController'

      // execute commande
      exec(`cli create-controller ${controller}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${controller}.js`, (result) => {
          // assert result
          expect(result).toBe(true)
          done()
        })

      })
    
    })

    it ('cli cc Folder/TestingController', (done) => {

      // updating controller
      controller = 'Folder/TestingController'

      // execute commande
      exec(`cli cc ${controller}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${controller}.js`, (result) => {
          // assert result
          expect(result).toBe(true)
          done()
        })

      })
    
    })

  })


  describe ('Delete Commands: ', () => {

    beforeEach (() => {

      /* 
      before each test, create a new directory with a new file
      in order to test deleting these files 
      */

      execSync(`cli cc ${controller}`)
      execSync(`cli cc Folder/${controller}`)

    })

    it ('cli delete-controller TestingController', (done) => {

      // execute commande
      exec(`cli delete-controller ${controller}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${controller}.js`, (result) => {
          // assert result
          expect(result).toBe(false)
          done()
        })

      })
    
    })

    it ('cli delete-controller Folder/TestingController', (done) => {

      // updating controller
      controller = 'Folder/TestingController'

      // execute commande
      exec(`cli delete-controller ${controller}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${controller}.js`, (result) => {
          // assert result
          expect(result).toBe(false)
          done()
        })

      })
    
    })

    it ('cli dc /Folder/TestingController', (done) => {

      // updating controller
      controller = '/Folder/TestingController'

      // execute commande
      exec(`cli dc ${controller}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${controller}.js`, (result) => {
          // assert result
          expect(result).toBe(false)
          done()
        })

      })
    
    })

    it ('cli dc TestingController', (done) => {

      // execute commande
      exec(`cli dc ${controller}`, (err) => {

        // error case
        if(err) throw err

        // check wther the file is created
        fs.exists(`${directory}${controller}.js`, (result) => {
          // assert result
          expect(result).toBe(false)
          done()
        })

      })
    
    })

  })

})

}