const User = require('../../Model/UserModel')
const faker = require('faker')
const bcrypt = require('bcrypt')
const config = require('config')

describe('UserModel: testing schema methods.', () => {

  it ('validPassword method should return true', async () => {

    // will test validPassword method using this example
    let password_example = 'Password Example'
    // will contain the hashed result of password_example
    let hashedPassword

    try {
      hashedPassword = await bcrypt.hash(password_example, config.get('bcrypt_salt_length')) 
    } catch (error) {
      throw error
    }

    const user = new User({
      name: faker.name.findName(),
      email: faker.internet.email(),
      // bacause pre.save on UserModel won't work without saving data
      // so this is just a mock
      password: hashedPassword
    }) 
    
    expect(await user.validPassword(password_example)).not.toBe(false)

  })

  it ('Using WRONG PASSWORD: validPassword should return false', async () => {

    // will test validPassword method using this example
    let password_example = 'Password Example'
    // will contain the hashed result of password_example
    let hashedPassword

    try {
      hashedPassword = await bcrypt.hash(password_example, config.get('bcrypt_salt_length')) 
    } catch (error) {
      throw error
    }

    const user = new User({
      name: faker.name.findName(),
      email: faker.internet.email(),
      // bacause pre.save on UserModel won't work without saving data
      // this is just a mock
      password: hashedPassword
    }) 
    
    expect(await user.validPassword('wrong password')).toBe(false)

  })

})