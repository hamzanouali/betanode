module.exports = {

  controller : 'AuthenticationController',
  prefix: '',

  get : [
    // All GET routes are listed here
    // route, controller method, middleware
    ['/login', 'render', 'VerifyAuth(notAuth)'],
    ['/home', 'renderHome', 'VerifyAuth(auth)'],
    ['/auth/github', 'login', 'VerifyAuth(notAuth).GithubPassportAuth'],
    ['/auth/github/callback', 'login', 'GithubPassportAuth'],
    ['/logout', 'logout', 'VerifyAuth(auth)']
    /* ['/facebook', 'login', 'FacebookPassportAuth'],
    ['/bitbucket', 'login', 'BitbucketPassportAuth'], */
  ],

  post: [
     // All POST routes are listed here
     ['/subscribe', 'subscribe', 'VerifyAuth(auth).VerifySubscription(test)'],
  ],

  put: [
    // All PUT routes are listed here
  ],

  delete: [
    // All DELETE routes are listed here
  ]

}