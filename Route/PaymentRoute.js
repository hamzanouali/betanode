module.exports = {

  controller : 'PaymentController',
  prefix: '',

  get : [
    // All GET routes are listed here
  ],

  post: [
     // All POST routes are listed here

     ['/payment', 'payment', 'VerifyAuth(auth)']
  ],

  put: [
    // All PUT routes are listed here
  ],

  delete: [
    // All DELETE routes are listed here
  ]

}