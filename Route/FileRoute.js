module.exports = {

  controller : 'FileController',
  prefix: '/files',

  get : [
    // All GET routes are listed here
    ['/form', 'renderform'],
    /**
     * for download full project as zip
     */
    ['/download-project', 'downloadProject', 'VerifyAuth(auth)'],
  ],

  post: [
     // All POST routes are listed here

    ['/create', 'create', 'VerifyAuth(auth)'],

    ['/create-one-page', 'createOnePage'],
     //['/download-workspace', 'downloadOneWorkspace'],
    /**
     * for uploading backup
     */
    ['/upload-backup', 'uploadBackup', 'VerifyAuth(auth).UploadBackupFile'],

    /**
     * for customizing css frameworks: bootstrap, bulma..etc
     */
    ['/customize-scss', 'customizeSCSS', 'VerifyAuth(auth)'],

    /**
     * for initializing css frameworks, like copying bootstrap from node_modules to user folder
     */
    ['/prepare-assets', 'prepareAssets', 'VerifyAuth(auth)'],
  
    ['/delete-asset', 'deleteAsset', 'VerifyAuth(auth)'],

    /**
     * delete workspace
     */
    ['/delete-workspace', 'deleteWorkspace', 'VerifyAuth(auth)'],

    ['/form', 'form']
     
  ],

  put: [
    // All PUT routes are listed here
  ],

  delete: [
    // All DELETE routes are listed here
  ]

}