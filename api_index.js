const { app, express } = require('./initExpress');

//const config = require('config')

const ejs = require('ejs');
app.set('view engine', 'ejs');

/**
 * global middleware contains csrf middleware
 * so it's must be before any app.use
 */
require('./Middleware/GlobalMiddleware');


/* if(!config.get('jwtPrivateKey')) { 
  console.error('jwtPrivateKey is not defined.')
  process.exit(1)
} */

process.on('unhandledPromiseRejection',(err) => {
  throw err
})

process.on('uncaughtException',(err) => {
  throw err
})

// link the Routes, Actions and middlewares
const core = require('./Core/Core')

core.initRouteActionPOST(app)
core.initRouteActionGET(app)

async function start() {

  // Listen the server
  let server = app.listen(7000, '127.0.0.1', () => console.log(`API ONLY, NO NUXT: Listening on port 7000...`))
  return server
}

start().then(server => {
  module.exports = server
})

