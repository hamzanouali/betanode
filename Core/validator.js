const Joi = require('joi')

module.exports = {

  /**
   * validate the payload then return an error in case of a failure
   * @param {Object} object object to be validated 
   * @param {Object} rules rules of validation
   * @param {Response}
   */
  validate (object, rules, res) {
    const {error, value} = Joi.validate(object, rules)
    if(error) {
      console.log('Validation Error: ', error.details)
      res.status(400).json(error.details)
      return error
    }
  }
  
}