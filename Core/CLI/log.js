module.exports = {
  /**
   * Log a green message to the console
   * @param {string} text 
   */
  logGreen(text) {
    // green background
    console.log("\x1b[42m", text)
    // reset
    console.log("\x1b[0m", '')
  },

  /**
   * Log a red message to the console
   * @param {string} text 
   */
  logRed(text) {
    // Red background
    console.log("\x1b[41m", text)
    // reset
    console.log("\x1b[0m", '')
  }

}