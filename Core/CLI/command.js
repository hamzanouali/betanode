#!/usr/bin/env node

var program = require('commander');

// requiring all Model commands
require('./commands/ModelCommands')

// requiring all Controller commands
require('./commands/ControllerCommands')

// requiring all Middleware commands
require('./commands/MiddlewareCommands')

// requiring all Route commands
require('./commands/RouteCommands')

// display the cli version
program
  .version('0.0.1', '-v, --version')

program.parse(process.argv)

