var program = require('commander');
const { StringDecoder } = require('string_decoder');
const decoder = new StringDecoder('utf8');
const fs = require('fs')
const log = require('../log')
const handleDirectory = require('./helpers/handleDirectory')

let directory = './Model/'

/**
 * Create a new Model
 */
program 
  .command('create-model <file_name>')
  .alias('cm')
  .description('Create a new Model.')
  .action((file_name) => {

    // extract the Model name from file_name
    const model_name = file_name.replace('Model', '')
    
    // read the sample of our models
    fs.readFile('./Core/CLI/Samples/ModelSample.js', (err, data) => {
      if (err) {
        log.logRed(err);
      }

      // decode buffer to string
      data = decoder.write(data)

      // replace 'model_name_should_be_her' with model_name
      data = data.replace('model_name_should_be_here', model_name)

      /* in case we are going to add a new directory + new route */
      // contains the new full directory and the file_name
      let handledObject = handleDirectory(file_name, directory)

      directory = handledObject.directory
      file_name = handledObject.file_name      

      // write the final result to our new model
      fs.writeFile(`${directory}${file_name}.js`, data, (err) => {

        if(err) {
          log.logRed(err);
        } else {
          log.logGreen(`${model_name} Model was created successfully.`)
        }

      })

    });
  })

/**
 * Delete Model
 */
program 
  .command('delete-model <file_name>')
  .alias('dm')
  .description('Delete a Model.')
  .action((file_name) => {
    

    /* in case we are going to add a new directory + new route */
    // contains the new full directory and the file_name
    let handledObject = handleDirectory(file_name, directory)

    directory = handledObject.directory
    file_name = handledObject.file_name

    // read the sample of our models
    fs.unlink(`${directory}${file_name}.js`, (err, data) => {
      
      if (err) {
        log.logRed(err);
      }else {
        log.logGreen(`${file_name} Model was deleted successfully.`)
      }

    });
  })  

module.exports = program