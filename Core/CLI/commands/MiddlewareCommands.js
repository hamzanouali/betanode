var program = require('commander');
const { StringDecoder } = require('string_decoder');
const decoder = new StringDecoder('utf8');
const fs = require('fs')
const log = require('../log')
const handleDirectory = require('./helpers/handleDirectory')

let directory = './Middleware/'

/**
 * Create a new Middleware
 */
program 
  .command('create-middleware <file_name>')
  .alias('cmi')
  .description('Create a new Middleware.')
  .action((file_name) => {

    // extract the Middleware name from file_name
    const middleware_name = file_name.replace('Middleware', '')
    
    // read the sample of our Middlewares
    fs.readFile('./Core/CLI/Samples/MiddlewareSample.js', (err, data) => {
      if (err) {
        log.logRed(err);
      }

      // decode buffer to string
      data = decoder.write(data)

      /* in case we are going to add a new directory + new route */
      // contains the new full directory and the file_name
      let handledObject = handleDirectory(file_name, directory)

      directory = handledObject.directory
      file_name = handledObject.file_name

      // write the final result to our new Middleware
      fs.writeFile(`${directory}${file_name}.js`, data, (err) => {

        if(err) {
          log.logRed(err);
        } else {
          log.logGreen(`${middleware_name} Middleware was created successfully.`)
        }

      })

    });
  })

/**
 * Delete Middleware
 */
program 
  .command('delete-middleware <file_name>')
  .alias('dmi')
  .description('Delete a Middleware.')
  .action((file_name) => {
    
    /* in case we are going to add a new directory + new route */
    // contains the new full directory and the file_name
    let handledObject = handleDirectory(file_name, directory)

    directory = handledObject.directory
    file_name = handledObject.file_name

    // read the sample of our Middlewares
    fs.unlink(`${directory}${file_name}.js`, (err, data) => {
      
      if (err) {
        log.logRed(err);
      }else {
        log.logGreen(`${file_name} Middleware was deleted successfully.`)
      }

    });
  })  

module.exports = program