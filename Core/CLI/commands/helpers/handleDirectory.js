const fs = require('fs')

/**
 * EXAMPLE:
 * handleDirectory('/Folder/FileName', './MainFolder/')
 * returns: { file_name: 'FileName', directory: './MainFolder/Folder/FileName' }
 * NOTE: in addition, this function will create the new directory '/Folder' if it does 
 * not exists
 * 
 * @param {string} file_name  must contain the file name to be created
 * @param {string} directory must contain the main directory 
 * @return {object} contains the full directory and file_name
 */
function handleDirectory (file_name, directory) {
  
  if(file_name.search('/') !== -1) {     
    // trim and remove the slash: (/directory/file.js) => (directory/file.js)
    let new_directory = file_name.replace(/^\/+|\/+$/gm, '')

    // extract the directory from file_name: (directory/file.js) => (['directory', 'file.js'])
    new_directory = new_directory.split('/')

    // updating the file_name: (directory/file.js) => (file.js)
    file_name = new_directory[1]

    new_directory = new_directory[0]

    if (!fs.existsSync(`${directory}${new_directory}`)){
      fs.mkdirSync(`${directory}${new_directory}`);
    }

    // updating directory
    directory = directory + new_directory + '/'

    return { directory, file_name }
  }
  // in case we don't need to change anything
  return { directory, file_name }
}

module.exports = handleDirectory