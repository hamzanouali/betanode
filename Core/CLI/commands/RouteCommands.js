var program = require('commander');
const { StringDecoder } = require('string_decoder');
const decoder = new StringDecoder('utf8');
const fs = require('fs')
const log = require('../log')
const handleDirectory = require('./helpers/handleDirectory')

// contains the Routes directory
// it may be changed
let directory = './Route/'

/**
 * Create a new Route
 */
program 
  .command('create-route <file_name>')
  .option('--controller <controller_name>', 'The name of the used controller by this route.')
  .option('--prefix <prefix>', 'The link\'s prefix.')
  .alias('cr')
  .description('Create a new Route.')
  .action((file_name, options) => {
    
    // extract the Route name from file_name
    const route_name = file_name.replace('Route', '')
    
    // read the sample of our Routes
    fs.readFile('./Core/CLI/Samples/RouteSample.js', (err, data) => {
      if (err) {
        log.logRed(err);
      }

      // decode buffer to string
      data = decoder.write(data)

      // insert the controller_name to the route
      if(options.controller) data = data.replace('controller_name', options.controller)

      // insert the link's prefix to the route
      data = data.replace('prefix_should_be_here', (options.prefix ? options.prefix : '') ) 

      /* in case we are going to add a new directory + new route */
      // contains the new full directory and the file_name
      let handledObject = handleDirectory(file_name, directory)

      directory = handledObject.directory
      file_name = handledObject.file_name

      // write the final result to our new Route
      fs.writeFile(`${directory}${file_name}.js`, data, async (err) => {

        if(err) {
          log.logRed(err);
        } else {
          /* Now we must push the new Route to /Route/all.js */
          // get the all.js array
          let all_js = require('../../../Route/all')
          // push the new route with its directory
          // we should push the route like that: 'folder/filename'
          // so we must remove './Route' from directory
          all_js.push(`${directory.replace('./Route/','')}${file_name}`)
          // stringify the result in order to write it back to all.js file
          all_js = JSON.stringify(all_js, null, 2)
          // adding some js code to the result
          all_js = `module.exports = ${all_js}`
          // write the result to /Route/all.js
          fs.writeFile(`./Route/all.js`, all_js, err => {

            if(err) {
              log.logRed(err)
              process.exit(1)
            }

            log.logGreen(`${route_name} Route was created successfully.`)

          })

        }

      })

    });
  })

/**
 * Delete Route
 */
program 
  .command('delete-route <file_name>')
  .alias('dr')
  .description('Delete a Route.')
  .action((file_name) => {
    
    /* in case we are going to add a new directory + new route */
    // contains the new full directory and the file_name
    let handledObject = handleDirectory(file_name, directory)

    directory = handledObject.directory
    file_name = handledObject.file_name

    // read the sample of our Routes
    fs.unlink(`${directory}${file_name}.js`, (err, data) => {
      
      if (err) {
        log.logRed(err);
      }else {
        log.logGreen(`${file_name} Route was deleted successfully.`)
      }

    });
  })  

module.exports = program