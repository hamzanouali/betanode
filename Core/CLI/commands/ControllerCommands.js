var program = require('commander');
const { StringDecoder } = require('string_decoder');
const decoder = new StringDecoder('utf8');
const fs = require('fs')
const log = require('../log')
const handleDirectory = require('./helpers/handleDirectory')

let directory = './Controller/'

/**
 * Create a new Controller
 */
program 
  .command('create-controller <file_name>')
  .alias('cc')
  .description('Create a new Controller.')
  .action((file_name) => {

    // extract the Controller name from file_name
    const controller_name = file_name.replace('Controller', '')

    // read the sample of our Controllers
    fs.readFile('./Core/CLI/Samples/ControllerSample.js', (err, data) => {
      if (err) {
        log.logRed(err);
      }

      // decode buffer to string
      data = decoder.write(data)

      /* in case we are going to add a new directory + new route */
      // contains the new full directory and the file_name
      let handledObject = handleDirectory(file_name, directory)

      directory = handledObject.directory
      file_name = handledObject.file_name

      // write the final result to our new Controller
      fs.writeFile(`${directory}${file_name}.js`, data, (err) => {

        if(err) {
          log.logRed(err);
        } else {
          log.logGreen(`${controller_name} Controller was created successfully.`)
        }

      })

    });
  })

/**
 * Delete Controller
 */
program 
  .command('delete-controller <file_name>')
  .alias('dc')
  .description('Delete a Controller.')
  .action((file_name) => {
    
    /* in case we are going to add a new directory + new route */
    // contains the new full directory and the file_name
    let handledObject = handleDirectory(file_name, directory)

    directory = handledObject.directory
    file_name = handledObject.file_name

    // read the sample of our Controllers
    fs.unlink(`${directory}${file_name}.js`, (err, data) => {
      
      if (err) {
        log.logRed(err);
      }else {
        log.logGreen(`${file_name} Controller was deleted successfully.`)
      }

    });
  })  

module.exports = program