const debug = require('debug')('app:core');

const allRoutes = require('../Route/all.js')

// will contain all the defined routes in Route directory
let bundledRoutes = []
/**
 * get all the defined routes in the Route directory
 */
function bundlingAllRoutes() {
  for (let i = 0; i < allRoutes.length; i++) {
    // require and push the routes into a signle bundle
    bundledRoutes.push(require(`../Route/${allRoutes[i]}`))
  }
}

/**
 * returns all the middlewares as an array for a specific route
 * @param {string} route 
 */
function allMiddlewares(route) {

  // groupe of middlewares
  let groupe = []

  // split the string by dots
  const middlewares = route[2] ? route[2].split('.') : []

  // loop throughout all middlewares
  for (let i = 0; i < middlewares.length; i++) {

    // extract middleware params
    let params = '';
    if(middlewares[i].includes('(')) {
      let middlewareNameAndParams = middlewares[i].split('(');
      // assign middleware name
      middlewares[i] = middlewareNameAndParams[0];
      // trim params
      params = middlewareNameAndParams[1].replace(')', '');
    }
    // end

    debug('Route - "'+ route[0] +': "require('+`../middleware/${middlewares[i]}`+')('+params+')');

    // require middleware
    const current_middleware = require(`../middleware/${middlewares[i]}`)(params);

    groupe.push(current_middleware)

  }
  return groupe
}

bundlingAllRoutes()

module.exports = {

  /**
   * creates all the get routes that are listed in the route for GET request
   * @param {object} app it's the returned express()
   */
  initRouteActionGET(app) {

    // loop throughout all the bundled routes
    for (let j = 0; j < bundledRoutes.length; j++) {
      
      let routes = bundledRoutes[j]
      // loop throughout all the routes for GET request
      for (let i = 0; i < routes.get.length; i++) {
        // current route
        let route = routes.get[i]
        // create a route 
        app.get(`${routes.prefix}${route[0]}`, allMiddlewares(route), (req, res) => {
          
          // will contain the controller
          let controller
          try {
            debug('Core: ' + `require(../Controller/${routes.controller}); `);
            // require the controller only when need it
            controller = require(`../Controller/${routes.controller}`); 
          } catch (error) {
            console.error(`Please define the controller name for this route: ${routes.prefix}${route[0]}`)
            process.exit(1)
          }

          if(controller[route[1]]) {
            controller[route[1]](req, res)
          } else {
            throw Error(`${route[1]} method does not exist in ${routes.controller} controller for the declared route: ${route[0]}.`)
          }

        })
      }

    }
  },

  /**
   * creates all the get routes that are listed in the route for POST request
   * @param {object} app it's the returned express()
   */
  initRouteActionPOST(app) {
    
    // loop throughout all the bundled routes
    for (let j = 0; j < bundledRoutes.length; j++) {
      
      let routes = bundledRoutes[j]

      // loop throughout all the routes for POST request
      for (let i = 0; i < routes.post.length; i++) {
        // current route
        let route = routes.post[i]

        // create a route 
        app.post(`${routes.prefix}${route[0]}`, allMiddlewares(route) , (req, res) => {

          // require the controller only when need it
          const controller = require(`../Controller/${routes.controller}`)
          // if the Method exists inside the conrtoller
          if(typeof controller[route[1]] == 'function') {
            controller[route[1]](req, res)
          } else {
            res.send(`You are calling ${route[1]} methode that does not exists inside ${routes.controller}.`)
          }
          
        })
      }

    }
  }

}