const { app, express } = require('./initExpress');
const { Nuxt, Builder } = require('nuxt');
// const consola = require('consola')
//const config = require('config')

const ejs = require('ejs');
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

/**
 * global middleware contains csrf middleware
 * so it's must be before any app.use
 */
require('./Middleware/GlobalMiddleware');


/*============================*/
/*============================*/
/*============================*/

//const config = require('config')

/* app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({limit: '50mb'})); */
/* const bodyParser= require('body-parser')
const multer = require('multer');
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({extended: true, limit: '50mb'}))

var upload = multer({ destination: './' });

const fs = require('fs');

app.post('/file/upload-backup', upload.single('file'), (req, res, next) => {
  const file = req.file
  if (!file) {
    const error = new Error('Please upload a file')
    error.httpStatusCode = 400
    return next(error)
  }
  
  // write file
  fs.writeFile('test-backup.json',file.buffer.toString() ,(err) => {
    if(err) throw err;

    // return backup as json response 
    res.json(file.buffer.toString());
  });
  
}) */


/* if(!config.get('jwtPrivateKey')) { 
  console.error('jwtPrivateKey is not defined.')
  process.exit(1)
} */

process.on('unhandledPromiseRejection',(err) => {
  throw err
})

process.on('uncaughtException',(err) => {
  throw err
})

// link the Routes, Actions and middlewares
const core = require('./Core/Core')

core.initRouteActionPOST(app)
core.initRouteActionGET(app)

/*============================*/
/*============================*/
/*============================*/

// Import and Set Nuxt.js options
const nuxtConfig = require('../nuxt.config.js')
nuxtConfig.dev = !(process.env.NODE_ENV === 'production')

async function start() {
  // Init Nuxt.js
  const nuxt = new Nuxt(nuxtConfig)

  const {
    host = process.env.HOST || '127.0.0.1',
    port = process.env.PORT || 3000
  } = nuxt.options.server

  // Build only in dev mode
  if (nuxtConfig.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  }

  // Give nuxt middleware to express
  app.use(nuxt.render)

  // Listen the server
  let server = app.listen(port, host, () => console.log(`Listening on port ${port}...`))
  return server
}

start().then(server => {
  module.exports = server
})

