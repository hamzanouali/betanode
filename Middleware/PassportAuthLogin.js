const User = require('../Model/UserModel')
const jwt = require('jsonwebtoken')
const config = require('config')
const validator = require('../Core/validator')
const Joi = require('joi')

const express = require('express')
const app = express()
var passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy; 

app.use(passport.initialize());

passport.use('local-login', new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password'
},
function(email, password, done) {
  
  User.findOne({ email: email }, async (err, user) => {

    if (err) { return done(err); }

    if (!user) {
      return done(null, false, { message: 'Incorrect email.' });
    }
    
    if (!await user.validPassword(password)) {
      return done(null, false, { message: 'Incorrect password.' });
    }

    const token = jwt.sign({ user }, config.get('jwtPrivateKey'))

    return done(null, token);
  });
}
));


module.exports = (req, res, next) => {

  const val = validator.validate(req.body, {
    email: Joi.string().email().required(),
    password: Joi.string().required() 
  },res)

  if(val) {
    return 
  }

  passport.authenticate('local-login', { session: false }, function(err, token, info) {

    if (err) { 
      return res.status(500)
      return next(err); 
    }
    if (!token) { return res.status(401).send('Wrong credentials.'); }

    res.json({ jwt : token })
  })(req, res, next)

};