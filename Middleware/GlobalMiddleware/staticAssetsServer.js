const { app, express } = require('../../initExpress');
const path = require('path');

/**
 * serving static assets to html in /views directory
 */
const staticAssetsObject = require('../../staticAssets');

for (const key in staticAssetsObject) {
  const directory  = staticAssetsObject[key];

  app.use(
    key, 
    express.static(path.join(__dirname, 
        path.join('../..', directory) 
      ))
  );
}
