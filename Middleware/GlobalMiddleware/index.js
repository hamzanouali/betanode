const { app, express } = require('../../initExpress');
require('./staticAssetsServer');

// body parser
var bodyParser = require('body-parser')
app.use(bodyParser.json({limit: '50mb'}));
var parseForm = bodyParser.urlencoded({extended: false, limit: '50mb'})
app.use(parseForm);

// cookies parser
app.use(require('cookie-parser')());

const csrf = require('csurf');
const csrfMiddleware = csrf({ cookie: true });
app.use(csrfMiddleware);

app.use((req, res, next) => {
  res.locals._csrf = req.csrfToken();
  res.locals.csrf_input =  `<input name="_csrf" type="hidden" value="${req.csrfToken()}">`;
  return next();
});


/**
 * include all global middlewares here
 * 
 * @todo: Write your code below
 */

/**
 * protect live-project and test-mode
 */
app.use((req, res, next) => {
  /* if(req.originalUrl.includes('/live/') || req.originalUrl.includes('/test/')) {
    
    return require('../VerifyAuth')('auth')(req, res, next);

  } else {
    return next();
  } */
  res.cookie('auth', { id: '12598426' }, { maxAge: 60*60*60*24*31 });
  return next();
});

/**
 * protect live-project, it must be paid
 */
app.use((req, res, next) => {
  if(req.originalUrl.includes('/live/')) {
    
    return require('../VerifySubscription')('paid')(req, res, next);

  } else {
    return next();
  }
});

