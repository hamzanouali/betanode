const debug = require('debug')('app:middleware');
const User = require('../Model/User');


/**
 * @todo: this middleware require VerifyAuth middleware
 * so make sur to append VerifyMiddleware before this 
 */
module.exports = (params = false) => {
  return (req, res, next) => { 

    // parse auth cookie
    try {
      req.cookies.auth = JSON.parse(req.cookies.auth);
    } catch (error) {
    }
    
    debug('VerifySubscription Middleware get called with params: '+ params);    
    debug('Subscription: ' + req.cookies.auth.subscription);  
    
    User.findOne({ where: { id: req.cookies.auth.id } })
      .then(user => {
        if(params === 'test' && user.subscription === params) {
          return next();
        } 
        
        /**
         * validate subscription delay
         */
        else if(params === 'paid') {
          debug('params === paid');
          debug('user.subscription: '+ user.subscription);
          
          if(user.subscription === 'test') return res.redirect('/home');
          else {
            /**
             * calculate the difference between the payment day and today
             * then do what must be done
             */
            try {
              const differenceBetweenDays = Math.round((new Date()-new Date(user.subscription))/(1000*60*60*24));
              
              debug('difference between Days: '+ differenceBetweenDays);
              
              if(differenceBetweenDays >= 31) {
                return res.redirect('/home');
              } else {
                return next();
              }
              
            } catch (error) {
              return res.redirect('/home');
            }
          }

          return res.redirect('/home');
        }
      }).catch(err => {
        return res.redirect('/500');
        throw err;
      });

  }
}