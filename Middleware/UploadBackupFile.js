const debug = require('debug')('app:middleware');
const multer = require('multer');

module.exports = (params = false) => {
  return (req, res, next) => { 
    
    try {
      req.cookies.auth = JSON.parse(req.cookies.auth).id;
    } catch (error) {
      
    }

    debug('UploadBackupFile Middleware');  
    const upload = multer({ destination: './static/' + req.cookies.auth.id });
    return upload.single('file')(req, res, next);

  }
}