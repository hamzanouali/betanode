const User = require('../Model/UserModel')
const jwt = require('jsonwebtoken')
const config = require('config')
const validator = require('../Core/validator')
const Joi = require('joi')

const express = require('express')
const app = express()
var passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy; 

app.use(passport.initialize());

passport.use('local-register', new LocalStrategy({
  usernameField: 'name',
  passwordField: 'password',
  passReqToCallback: true
},
function(req, name, password, done) {

  User.findOne({ email: req.body.email }, function (err, user) {

    if (err) { return done(err); }

    return done(null, user)
    
  });
}
));


module.exports = (req, res, next) => {

  // validating inputs
  const val = validator.validate(req.body, {
    name: Joi.string().required(),
    email: Joi.string().email().required(),
    password: Joi.string().required(),
    passwordConfirmation: Joi.string().valid(Joi.ref('password')).required()
  },res)

  if(val) return 

  passport.authenticate('local-register', { session: false }, async (err, user, info) => {
    if (err) { return next(err); }
    if (user) { return res.status(403).send('These credentials already exists.'); }
    
    // create user
    const result = new User({
      name: req.body.name,
      email: req.body.email,
      password: req.body.password
    })

    // save user
    await result.save()

    // return the created user
    res.status(201).json(result) 

  })(req, res, next)

};