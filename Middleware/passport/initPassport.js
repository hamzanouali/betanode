const {app, express} = require('../../initExpress')

const passport = require('passport');
app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

const PassportMiddlewareCallback = require('./PassportMiddlewareCallback')

module.exports = { passport, PassportMiddlewareCallback }