const { Sequelize, sequelize } = require('../Model/sequelize');

/**
 * Require all Models here
 **************************************************************/
const User = require('../Model/User');
const Payment = require('../Model/Payment');
/* const Plan = require('../Model/Plan');
const BillingAgreement = require('../Model/BillingAgreement'); */



/***************************************************************
 * Creating tables
 */
sequelize.sync();