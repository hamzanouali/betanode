const axios = require('axios');

module.exports = {
  
  listPlans(accessToken) {
    return new Promise((resolve, reject) => {
      axios({
        url: 'https://api.sandbox.paypal.com/v1/billing/plans?page_size=10&page=1&total_required=true',
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${accessToken['access-token']}`
        }
      })
      .then(response => {
        resolve(response.data);
      })
      .catch(err => {
        reject(err);
      });

    })
  }

}