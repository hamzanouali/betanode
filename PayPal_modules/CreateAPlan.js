const billingPlanAttribs = {
    name: 'Basic',
    description: 'Monthly plan',
    type: 'fixed',
    payment_definitions: [{
      name: 'Basic plan',
      type: 'REGULAR',
      frequency_interval: '1',
      frequency: 'MONTH',
      cycles: '11',
      amount: {
        currency: 'USD',
        value: '19.99'
      }
    }],
    merchant_preferences: {
      cancel_url: 'http://localhost:7000/cancel',
      return_url: 'http://localhost:7000/success',
      max_fail_attempts: '0',
      auto_bill_amount: 'YES',
      initial_fail_amount_action: 'CONTINUE'
    }
  };

paypal.billingPlan.create(billingPlanAttribs, function (error, billingPlan){
  var billingPlanUpdateAttributes;

  if (error){
    console.error(JSON.stringify(error));
    throw error;
  } else {
    // Create billing plan patch object
    billingPlanUpdateAttributes = [{
      op: 'replace',
      path: '/',
      value: {
        state: 'ACTIVE'
      }
    }];

    // Activate the plan by changing status to active
    paypal.billingPlan.update(billingPlan.id, billingPlanUpdateAttributes, function(error, response){
      if (error){
        console.error(JSON.stringify(error));
        throw error;
      } else {
        console.log('Billing plan created under ID: ' + billingPlan.id);
        res.end('Billing plan created under ID: ' + billingPlan.id);
        //createBillingAgreement(billingPlan.id);
      }
    });
  }
});